<?php

Route::get('/', 'DemoPageController@index');
Route::post('/products', 'ProductsController@create');
Route::get('/products', 'ProductsController@index');
