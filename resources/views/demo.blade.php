<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Demo</title>
    </head>
    <body>
        <div class="flex-center position-ref full-height" id="app">
            <demo-page></demo-page>
        </div>
        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>
