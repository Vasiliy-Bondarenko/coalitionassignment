<?php

namespace App\Http\Controllers;

use App\DemoStorage;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * @var DemoStorage
     */
    private $storage;

    public function __construct(DemoStorage $storage)
    {
        $this->storage = $storage;
    }

    public function create(Request $request)
    {
        $data = $this->validate($request, [
            "price"        => "required|integer",
            "product_name" => "required",
            "quantity"     => "required|integer",
        ]);

        $data["submited"] = now()->toDateTimeString();

        $this->storage->append($data);

        return response(["status" => "ok"], 201);
    }

    public function index()
    {
        return $this->storage->getAll();
    }
}
