<?php namespace App;

use Storage;

class DemoStorage
{
    protected $path = "demo_data.json";

    public function append(array $obj)
    {
        $data = $this->getAll();
        $data[] = $obj;

        Storage::put($this->path, json_encode($data));
    }

    public function getAll(): array
    {
        if (!Storage::exists($this->path)) {
            return [];
        }
        return json_decode(Storage::get($this->path));
    }
}